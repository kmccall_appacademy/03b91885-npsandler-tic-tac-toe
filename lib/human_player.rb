class HumanPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    system('clear')
    @board = board
    board.grid.each do |row|
      puts row.join("  |  ")
      puts '------------'
    end
  end

  def get_move
    puts "Where to mark? (row, col)"
    gets.chomp.split(',').map(&:to_i)
  end

end
