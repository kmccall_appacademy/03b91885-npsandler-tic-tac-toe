class ComputerPlayer
   attr_accessor :name, :mark, :board

  def initialize
    @name = "computer"
  end

  def get_move
    available_moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos = row, col
        available_moves << pos if board.empty?(pos)
      end
    end

    available_moves.each do |pos|
        board[pos] = self.mark
        if board.winner
          board[pos] = nil
          return pos
        else
          board[pos] = nil
        end
        return available_moves.sample
      end
  end
end
require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :player1, :player2, :board

  def initialize
    @board = Board.new
    @player1 = ComputerPlayer.new
    @player2 = ComputerPlayer.new
    player1.mark = :X
    player2.mark = :O
    @current_player = @player1
  end

  def switch_players!
    if @current_player == @player1
      @current_player = @player2
    else
      @current_player = @player1
    end
  end

  def play_turn
    pos = @current_player.get_move
    until @board.empty?(pos)
      puts "That was an invalid move, please select again."
      pos = @current_player.get_move
    end
    @board.place_mark(pos, @current_player.mark)
  end

  def play

    until @board.over?
      @board.display
      play_turn
      switch_players!
    end

    if @board.winner.nil?
      puts "Cats Game"
    else
      switch_players!
      puts "#{@current_player.name} is the winner!"
    end
    @board.display
  end

end
class Board

  attr_accessor :grid

  def initialize
    @grid = Array.new(3) {Array.new(3)}
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    (@grid + cols + diagonals).each do |set|
      return :X if set == [:X,:X,:X]
      return :O if set == [:O,:O,:O]
    end
    nil
  end

  def over?
    @grid.flatten.none? { |pos| pos.nil? } || winner
  end

  def cols
    cols = []
    (0..2).each do |row|
      (0..2).each do |col|
        cols << @grid[col][row]
      end
    end
    cols
  end

  def diagonals
    diag_one = [@grid[0][0],@grid[1][1],@grid[2][2]]
    diag_two = [@grid[2][0],@grid[1][1],@grid[0][2]]
    diagonals = [diag_one, diag_two]
    diagonals
  end

  def display
    puts "    0,  1,  2"
    puts @grid.each_with_index.map {|row, index|"#{index} #{row.to_s}"}
  end

  def [](pos)
    row, col = pos
    return @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

end
