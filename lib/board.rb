class Board
  attr_accessor :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid  = grid
  end

  def place_mark(pos, mark)
    if pos.first.class == Fixnum
      @grid[pos.first][pos.last] = mark
    elsif pos.first.class == Array
      pos.each do |one_pos|
        @grid[one_pos.first][one_pos.last] = mark
      end
    end
  end

  def empty?(pos)
    if @grid[pos.first][pos.last] == nil
      return true
    else
      false
    end
  end

  def winner
    if win_row?(@grid) != nil
      return win_row?(@grid)
    elsif win_left_diagonal?(@grid) != nil
      return win_left_diagonal?(@grid)
    elsif win_right_diagonal?(@grid) != nil
      return win_right_diagonal?(@grid)
    elsif win_column?(@grid) != nil
      return win_column?(@grid)
    end

    nil
  end

  def win_row?(grid)
    grid.each do |row|
      if row.all? {|mark| mark == row[0]}
        return row[0]
      end
    end

    nil
  end

  def win_column?(grid)
    indices = (0..@grid.first.length - 1)
    collected_marks = []

    indices.each do |index|
      @grid.each do |row|
        collected_marks << row[index]
      end

      if collected_marks.all? {|mark| mark == collected_marks[0]}
        return collected_marks[0]
      end
    end

    nil
  end

  def win_left_diagonal?(grid)
    max_index = grid.first.length - 1
    indices_pool = (0..max_index).to_a
    right_diagnoal_coordinates = []

    indices_pool.each do |index|
      right_diagnoal_coordinates << [index, index]
    end

    collected_marks = []

    right_diagnoal_coordinates.each do |coordinate|
      collected_marks << @grid[coordinate.first][coordinate.last]
    end

    if collected_marks.all? {|mark| collected_marks[0] == mark}
      return collected_marks.first
    end

    nil
  end


  def win_right_diagonal?(grid)
    max_index = grid.first.length - 1
    indices_pool = (0..max_index).to_a
    indices_pool_reversed = indices_pool.reverse
    left_diagnoal_coordinates = []
    i = 0

    while i < indices_pool.length
      left_diagnoal_coordinates << [indices_pool[i], indices_pool_reversed[i]]
      i += 1
    end

    collected_marks = []

    left_diagnoal_coordinates.each do |coordinate|
      collected_marks << @grid[coordinate.first][coordinate.last]
    end

    if collected_marks.all? {|mark| collected_marks[0] == mark}
      return collected_marks.first
    end

    nil
  end

  def over?
    if self.winner != nil
      return true
    end

    @grid.each do |row|
      row.each do |mark|
        return false if mark == nil
      end
    end

    true
  end
end
